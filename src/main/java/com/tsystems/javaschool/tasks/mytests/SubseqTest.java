package com.tsystems.javaschool.tasks.mytests;

import com.tsystems.javaschool.tasks.subsequence.Subsequence;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class SubseqTest {
    public static void main(String[] args) {
        Subsequence ss = new Subsequence();
        List list1 = Arrays.asList(1);
        List list2 = Arrays.asList(2,1);
        System.out.println(ss.find(list1, list2));
    }
}
