package com.tsystems.javaschool.tasks.mytests;

import com.tsystems.javaschool.tasks.calculator.Calculator;

public class CalcTest {
    public static void main(String[] args) {
        Calculator calc = new Calculator();
        System.out.println(calc.evaluate("10/2"));
    }
}
