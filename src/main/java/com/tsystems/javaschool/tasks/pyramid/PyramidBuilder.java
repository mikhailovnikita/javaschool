package com.tsystems.javaschool.tasks.pyramid;

import java.util.Iterator;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        int h = findHeight(inputNumbers.size());
        if(Integer.MAX_VALUE / 2 < h) throw new CannotBuildPyramidException();
        int w = h * 2 - 1;
        int[][] pyramid = new int[h][w];
        try {
            inputNumbers.sort(Integer::compareTo);
        } catch (NullPointerException e) {
            throw new CannotBuildPyramidException();
        }

        int border = h - 1;
        Iterator<Integer> it = inputNumbers.iterator();

        for (int i = 0; i < h; i++) {
            int pos = border;

            while (true) {
                pyramid[i][pos] = it.next();
                pos += 2;
                if (pos + border >= w) break;
            }
            border--;
        }

        return pyramid;
    }

    private static int findHeight(int size) {
        if (size == 0) throw new CannotBuildPyramidException();
        long overflowChecker;
        int height = 1;
        int totalElements = 1;

        while (true) {
            if (totalElements == size) return height;
            if (totalElements > size) throw new CannotBuildPyramidException();

            height++;
            overflowChecker = totalElements;
            if(overflowChecker + height > Integer.MAX_VALUE) throw new CannotBuildPyramidException();
            totalElements += height;
        }
    }

}
