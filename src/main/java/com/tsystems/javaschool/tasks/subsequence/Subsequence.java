package com.tsystems.javaschool.tasks.subsequence;

import java.util.Iterator;
import java.util.List;
import java.util.Objects;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        if(Objects.isNull(x) || Objects.isNull(y)) throw new IllegalArgumentException();
        if(y.size() < x.size()) return false;
        if (x.isEmpty()) return true;

        Iterator itX = x.iterator();
        Iterator itY = y.iterator();

        Object elX = itX.next();

        while (itY.hasNext()) {
            Object elY = itY.next();

            if (elX.equals(elY)) {
                if (!itX.hasNext()) return true;
                elX = itX.next();
            }
        }


        return false;
    }
}
