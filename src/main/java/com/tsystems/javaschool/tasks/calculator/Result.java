package com.tsystems.javaschool.tasks.calculator;

/**
 * Class is used to store data while parsing
 */
public class Result {
    public double acc;
    public String rest;

    public Result(double v, String r) {
        this.acc = v;
        this.rest = r;
    }
}

