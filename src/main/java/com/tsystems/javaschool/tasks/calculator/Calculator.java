package com.tsystems.javaschool.tasks.calculator;


import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Objects;

public class Calculator {
    private final HashSet<Character> allowedChars = new HashSet<>(Arrays
            .asList('+','-','*','/','.', '(', ')'));
    /**
     * Recursive descent parser
     * Evaluate statement represented as string.
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        if (Objects.isNull(statement) || statement.isEmpty()) return null;
        if (!parenthesisBalanced(statement)) return null;

        //multiple minuses are not allowed? strange
        if (!minusesValid(statement)) return null;

        //deleting all whitespaces and special characters
        statement = statement.replaceAll("\\s", "");

        try {
            Result res = addSub(statement);
            if (Double.isInfinite(res.acc) || Double.isNaN(res.acc)) return null;

            //removing ".0" for integer numbers
            if (res.acc == Math.round(res.acc)) return Long.toString((long) res.acc);

            DecimalFormat df = new DecimalFormat("#.####");
            df.setRoundingMode(RoundingMode.HALF_UP);
            return df.format(res.acc);
        } catch (ParseException e) {
            return null;
        }

    }

    private Result addSub(String statement) throws ParseException {
        Result current = mulDiv(statement);
        double acc = current.acc;

        while (!current.rest.isEmpty()) {
            if (!(current.rest.charAt(0) == '+' || current.rest.charAt(0) == '-')) break;
            char sign = current.rest.charAt(0);
            current = mulDiv(current.rest.substring(1));

            switch (sign) {
                case '+':
                    acc += current.acc;
                    break;
                case '-':
                    acc -= current.acc;
                    break;
            }
        }

        return new Result(acc, current.rest);
    }

    private Result mulDiv(String statement) throws ParseException {
        Result current = bracket(statement);

        double acc = current.acc;

        while (true) {
            if (current.rest.isEmpty()) return current;

            char sign = current.rest.charAt(0);
            if ((sign != '*' && sign != '/')) return current;

            String next = current.rest.substring(1);
            Result right = bracket(next);

            switch (sign) {
                case '*':
                    acc *= right.acc;
                    break;
                case '/':
                    acc /= right.acc;
                    break;
            }

            current = new Result(acc, right.rest);
        }
    }

    private Result bracket(String statement) throws ParseException {
        if (statement.charAt(0) == '(') {
            Result r = addSub(statement.substring(1));
            if (!r.rest.isEmpty() && r.rest.charAt(0) == ')') {
                r.rest = r.rest.substring(1);
            } else {
                throw new ParseException("Missing bracket");
            }
            return r;
        }
        return number(statement);
    }

    private Result number(String statement) throws ParseException {
        int i = 0;
        int dotCounter = 0;
        boolean negative = false;


        if (statement.charAt(0) == '-') {
            negative = true;
            statement = statement.substring(1);
        }


        while (i < statement.length() && (Character.isDigit(statement.charAt(i))
                || statement.charAt(i) == '.')) {

            if (statement.charAt(i) == ',') throw new ParseException("Commas are not allowed");

            if (statement.charAt(i) == '.' && ++dotCounter > 1) {
                throw new ParseException("Two or more dots in a number");
            }

            i++;

            //avoiding errors with parsing numbers which contain not allowed characters
            // e.g. "1+2|3"
            if(i < statement.length()){
                char c = statement.charAt(i);
                if(!Character.isDigit(c) && !allowedChars.contains(c)){
                    throw new ParseException("Char [" + c + "] is not allowed");
                }
            }
        }

        if (i == 0) {
            throw new ParseException("Number expected in [" + statement + "]");
        }

        double doublePart = Double.parseDouble(statement.substring(0, i));
        if (negative) doublePart = -doublePart;

        String restPart = statement.substring(i);
        return new Result(doublePart, restPart);
    }

    //checking brackets balance
    private boolean parenthesisBalanced(String s) {
        char[] chars = s.toCharArray();
        int balance = 0;
        for (char c : chars) {
            if (c == '(') balance++;
            if (c == ')') balance--;
            if (balance < 0) return false;
        }

        return (balance == 0);
    }

    //checking for double minuses
    //e.g. "2--3" is not a valid expression
    private boolean minusesValid(String s) {
        char[] chars = s.toCharArray();
        boolean previousMinus = false;

        for (char c : chars) {
            if (c == '-') {
                if (previousMinus) return false;
                previousMinus = true;
            } else {
                previousMinus = false;
            }
        }

        return true;
    }
}
