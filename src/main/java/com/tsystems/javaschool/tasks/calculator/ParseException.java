package com.tsystems.javaschool.tasks.calculator;

class ParseException extends Exception {
    ParseException(String s) {
        super(s);
    }
}
